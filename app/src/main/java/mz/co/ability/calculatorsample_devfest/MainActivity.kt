package mz.co.ability.calculatorsample_devfest

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

/**
 * Created by Paulo Enoque on 11/23/2017.
 */
class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var numberOne: Double
        var numberTwo: Double

        sum.setOnClickListener {
            numberOne = first_number.text.toString().toDouble()
            numberTwo = second_number.text.toString().toDouble()
            result.text = numberOne plus numberTwo
        }

        div.setOnClickListener {
            numberOne = first_number.text.toString().toDouble()
            numberTwo = second_number.text.toString().toDouble()
            result.text = numberOne dividedBy numberTwo
        }


    }

    infix fun Double.plus(number: Double) = (this + number).toString()
    infix fun Double.dividedBy(number: Double) =
            if (number != 0.0) (this / number).toString() else "Invalid operation"
}